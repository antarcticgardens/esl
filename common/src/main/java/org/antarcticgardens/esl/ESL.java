package org.antarcticgardens.esl;

import org.antarcticgardens.esl.energy.BlockEnergyStorageManager;
import org.antarcticgardens.esl.energy.ItemEnergyStorageManager;
import org.antarcticgardens.esl.transaction.TransactionStack;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.function.Supplier;

public abstract class ESL {
    public static Logger LOGGER = LogManager.getLogger();
    
    private static ESL INSTANCE;
    
    protected ESL() {
        INSTANCE = this;
    }
    
    public abstract BlockEnergyStorageManager getBlockEnergyStorageManager();
    public abstract ItemEnergyStorageManager getItemEnergyStorageManager();
    
    public abstract Supplier<TransactionStack> getTransactionStackSupplier();
    
    public static ESL getInstance() {
        return INSTANCE;
    }
}
