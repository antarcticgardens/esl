package org.antarcticgardens.esl.energy;

import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.antarcticgardens.esl.ESL;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

public abstract class BlockEnergyStorageManager {
    private final Map<BlockEntityType<? extends BlockEntity>, BiFunction<BlockEntity, Direction, EnergyStorage>> blockEntityProviders = new HashMap<>();
    
    @SuppressWarnings("unchecked")
    public <T extends BlockEntity> void registerForBlockEntity(BiFunction<T, Direction, EnergyStorage> provider, BlockEntityType<T> blockEntityType) {
        if (blockEntityProviders.containsKey(blockEntityType)) {
            ESL.LOGGER.trace("Duplicate energy storage registration for " + blockEntityType);
            return;
        }

        blockEntityProviders.put(blockEntityType, (BiFunction<BlockEntity, Direction, EnergyStorage>) provider);
        platformRegisterForBlockEntity(provider, blockEntityType);
    }

    public boolean isBlockEntityRegisteredInESL(BlockEntityType<?> blockEntityType) {
        return blockEntityProviders.containsKey(blockEntityType);
    }

    public EnergyStorage find(World world, BlockPos pos, @Nullable Direction direction) {
        BlockEntity be = world.getBlockEntity(pos);

        if (be != null) {
            var provider = blockEntityProviders.get(be.getType());

            if (provider != null) {
                EnergyStorage storage = provider.apply(be, direction);

                if (storage != null) {
                    return storage;
                }
            }
        }
        
        return platformFind(world, pos, direction);
    }
    
    protected abstract <T extends BlockEntity> void platformRegisterForBlockEntity(BiFunction<T, Direction, EnergyStorage> provider,
                                                                                BlockEntityType<T> blockEntityType);

    protected abstract EnergyStorage platformFind(World world, BlockPos pos, @Nullable Direction direction);
}
