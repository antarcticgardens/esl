package org.antarcticgardens.esl.energy;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.antarcticgardens.esl.transaction.Transaction;
import org.antarcticgardens.esl.transaction.TransactionContext;

import java.util.ArrayList;
import java.util.List;

public class EnergyHelper {
    public static long moveEnergy(EnergyStorage from, EnergyStorage to, long amount, TransactionContext txn) {
        try (Transaction test = txn.openNested()) {
            long finalAmount = to.insert(amount, test);
            finalAmount = from.extract(finalAmount, test);
            test.abort();
            to.insert(finalAmount, txn);
            from.extract(finalAmount, txn);
            return finalAmount;
        }
    }
    
    public static long insertToSurrounding(EnergyStorage storage, BlockPos pos, World world, long maxAmount, TransactionContext txn) {
        List<EnergyStorage> directionStorages = new ArrayList<>();
        for (Direction dir : Direction.values()) {
            EnergyStorage directionStorage = EnergyStorage.findForBlock(world, pos.offset(dir), dir.getOpposite());
            if (directionStorage != null) {
                directionStorages.add(directionStorage);
            }
        }
        
        int storagesLeft = directionStorages.size();
        long inserted = 0;
        for (EnergyStorage directionStorage : directionStorages) {
            inserted += moveEnergy(storage, directionStorage, maxAmount / storagesLeft--, txn);
        }
        
        return inserted;
    }
}
