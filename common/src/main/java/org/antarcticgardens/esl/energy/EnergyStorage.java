package org.antarcticgardens.esl.energy;

import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.ItemConvertible;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.antarcticgardens.esl.ESL;
import org.antarcticgardens.esl.transaction.TransactionContext;
import org.antarcticgardens.esl.util.ItemHolder;
import org.jetbrains.annotations.Nullable;

import java.util.function.BiFunction;
import java.util.function.Function;

public interface EnergyStorage {
    long insert(long amount, TransactionContext context);
    long extract(long amount, TransactionContext context);
    long getStoredEnergy();
    long getCapacity();
    
    default boolean supportsExtraction() {
        return true;
    }
    
    default boolean supportsInsertion() {
        return true;
    }
    
    static EnergyStorage findForBlock(World world, BlockPos pos, @Nullable Direction direction) {
        return ESL.getInstance().getBlockEnergyStorageManager().find(world, pos, direction);
    }
    
    static <T extends BlockEntity> void registerForBlockEntity(BiFunction<T, Direction, @Nullable EnergyStorage> provider, BlockEntityType<T> blockEntityType) {
        ESL.getInstance().getBlockEnergyStorageManager().registerForBlockEntity(provider, blockEntityType);
    }
    
    static EnergyStorage findForItem(ItemHolder holder) {
        return ESL.getInstance().getItemEnergyStorageManager().find(holder);
    }
    
    static void registerForItem(Function<ItemHolder, EnergyStorage> provider, ItemConvertible item) {
        ESL.getInstance().getItemEnergyStorageManager().register(provider, item);
    }
}
