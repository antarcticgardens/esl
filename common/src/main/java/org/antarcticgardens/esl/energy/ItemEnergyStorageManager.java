package org.antarcticgardens.esl.energy;

import net.minecraft.item.ItemConvertible;
import org.antarcticgardens.esl.ESL;
import org.antarcticgardens.esl.util.ItemHolder;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public abstract class ItemEnergyStorageManager {
    private final Map<ItemConvertible, Function<ItemHolder, EnergyStorage>> providers = new HashMap<>();
    
    public void register(Function<ItemHolder, EnergyStorage> provider, ItemConvertible item) {
        if (providers.containsKey(item)) {
            ESL.LOGGER.trace("Duplicate energy storage registration for " + item);
            return;
        }

        providers.put(item, provider);
        platformRegister(provider, item);
    }
    
    public boolean isItemRegisteredInESL(ItemConvertible item) {
        return providers.containsKey(item);
    }
    
    public EnergyStorage find(ItemHolder holder) {
        var provider = providers.get(holder.getItem());

        if (provider != null) {
            EnergyStorage storage = provider.apply(holder);

            if (storage != null) {
                return storage;
            }
        }
        
        return platformFind(holder);
    }
    
    protected abstract void platformRegister(Function<ItemHolder, EnergyStorage> provider, ItemConvertible item);
    protected abstract EnergyStorage platformFind(ItemHolder holder);
}
