package org.antarcticgardens.esl.transaction;

public interface Transaction extends AutoCloseable, TransactionContext {
    void commit();
    void abort();
    boolean isOpen();

    @Override
    void close();
}
