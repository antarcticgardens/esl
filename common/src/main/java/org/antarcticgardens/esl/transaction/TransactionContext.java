package org.antarcticgardens.esl.transaction;

public interface TransactionContext {
    Transaction openNested();
    int getNestingDepth();
    Transaction getOpenTransaction(int depth);
    void addCloseCallback(CloseCallback callback);
    void addOuterCloseCallback(OuterCloseCallback callback);

    @FunctionalInterface
    interface CloseCallback {
        void onClose(TransactionContext context, TransactionResult result);
    }
    
    @FunctionalInterface
    interface OuterCloseCallback {
        void onOuterClose(TransactionResult result);
    }
}
