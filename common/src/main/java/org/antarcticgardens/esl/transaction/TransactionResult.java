package org.antarcticgardens.esl.transaction;

public enum TransactionResult {
    COMMITTED,
    ABORTED;
}
