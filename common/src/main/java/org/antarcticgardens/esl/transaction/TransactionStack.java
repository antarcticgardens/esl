package org.antarcticgardens.esl.transaction;

import org.antarcticgardens.esl.ESL;

public abstract class TransactionStack {
    private static final ThreadLocal<TransactionStack> LOCAL = ThreadLocal.withInitial(ESL.getInstance().getTransactionStackSupplier());

    public abstract Transaction openOuter();
    public abstract Transaction getCurrent();
    public abstract int getDepth();

    public static TransactionStack get() {
        return LOCAL.get();
    }
}
