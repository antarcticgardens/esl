package org.antarcticgardens.esl.transaction;

public class TransactionUtil {
    public static Transaction openOuterOrNested() {
        if (TransactionStack.get().getDepth() == -1) {
            return TransactionStack.get().openOuter();
        } else {
            return TransactionStack.get().getCurrent().openNested();
        }
    }
}
