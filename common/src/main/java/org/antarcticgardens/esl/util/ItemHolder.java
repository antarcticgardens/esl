package org.antarcticgardens.esl.util;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;

public interface ItemHolder {
    Item getItem();
    long getAmount();
    NbtCompound getNbt();
    
    void setItem(Item item);
    void setAmount(long amount);
    void setNbt(NbtCompound nbt);
    
    void fromItemStack(ItemStack stack);
    ItemStack toItemStack();
}
