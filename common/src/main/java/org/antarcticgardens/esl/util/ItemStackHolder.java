package org.antarcticgardens.esl.util;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;

public class ItemStackHolder implements ItemHolder {
    private ItemStack stack;
    
    public ItemStackHolder(ItemStack stack) {
        this.stack = stack;
    }
    
    @Override
    public Item getItem() {
        return stack.getItem();
    }

    @Override
    public long getAmount() {
        return stack.getCount();
    }

    @Override
    public NbtCompound getNbt() {
        return stack.getNbt();
    }

    @Override
    public void setItem(Item item) {
        stack = new ItemStack(item, Mth.longToIntSaturated(getAmount()));
        stack.setNbt(getNbt());
    }

    @Override
    public void setAmount(long amount) {
        stack.setCount(Mth.longToIntSaturated(amount));
    }

    @Override
    public void setNbt(NbtCompound nbt) {
        stack.setNbt(nbt);
    }

    @Override
    public void fromItemStack(ItemStack stack) {
        this.stack = stack;
    }

    @Override
    public ItemStack toItemStack() {
        return stack;
    }
}
