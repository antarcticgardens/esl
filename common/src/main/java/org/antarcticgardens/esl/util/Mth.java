package org.antarcticgardens.esl.util;

public class Mth {
    public static int longToIntSaturated(long value) {
        return (int) Math.max(Math.min(value, Integer.MAX_VALUE), Integer.MIN_VALUE);
    }
}
