package org.antarcticgardens.esl.test;

import org.antarcticgardens.esl.energy.SimpleEnergyStorage;
import org.antarcticgardens.esl.transaction.Transaction;
import org.antarcticgardens.esl.transaction.TransactionStack;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public abstract class SimpleEnergyStorageTestBase {
    @Test
    void idkSomeTest() {
        SimpleEnergyStorage storage = new SimpleEnergyStorage(100);
        assertEquals(100, storage.getCapacity());
        assertEquals(0, storage.getStoredEnergy());
        
        assertTrue(storage.supportsExtraction());
        assertTrue(storage.supportsInsertion());
        
        try (Transaction outer = TransactionStack.get().openOuter()) {
            assertThrowsExactly(IllegalStateException.class, TransactionStack.get()::openOuter);
            
            assertEquals(10, storage.insert(10, outer));
            assertEquals(10, storage.getStoredEnergy());
            
            try (Transaction nested1 = outer.openNested()) {
                assertEquals(20, storage.insert(20, nested1));
                assertEquals(30, storage.getStoredEnergy());
                
                // Transaction is automatically aborted
            }

            assertEquals(10, storage.getStoredEnergy());

            try (Transaction nested1 = outer.openNested()) {
                assertEquals(10, storage.insert(10, nested1));
                assertEquals(20, storage.getStoredEnergy());
                
                try (Transaction nested2 = nested1.openNested()) {
                    try (Transaction nested3 = nested2.openNested()) {
                        storage.setStoredEnergy(69);
                        assertEquals(69, storage.getStoredEnergy());
                        
                        assertEquals(69, storage.extract(200, nested3));
                        assertEquals(0, storage.getStoredEnergy());
                        
                        nested3.commit();
                    }

                    assertEquals(0, storage.getStoredEnergy());
                    
                    assertEquals(100, storage.insert(1000, nested2));
                    assertEquals(100, storage.getStoredEnergy());
                    
                    nested2.commit();
                }

                assertEquals(100, storage.getStoredEnergy());
                nested1.commit();
            }

            try (Transaction nested1 = outer.openNested()) {
                storage.setStoredEnergy(100000);
                assertEquals(100, storage.getStoredEnergy());

                storage.setStoredEnergy(-100);
                assertEquals(0, storage.getStoredEnergy());
            }

            assertEquals(0, storage.getStoredEnergy());
        }
    }
}
