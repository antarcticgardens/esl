package org.antarcticgardens.esl.test;

import org.antarcticgardens.esl.transaction.Transaction;
import org.antarcticgardens.esl.transaction.TransactionStack;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public abstract class TransactionStackTestBase {
    @Test
    void outerTransaction() {
        assertEquals(-1, TransactionStack.get().getDepth());

        Transaction outer = TransactionStack.get().openOuter();
        
        assertTrue(outer::isOpen);
        assertEquals(0, TransactionStack.get().getDepth());
        
        assertThrowsExactly(IllegalStateException.class, TransactionStack.get()::openOuter);
        
        assertDoesNotThrow(outer::commit);
        
        assertFalse(outer::isOpen);
        assertEquals(-1, TransactionStack.get().getDepth());
    }
    
    @Test
    void nestedTransaction() {
        assertEquals(-1, TransactionStack.get().getDepth());

        Transaction outer = TransactionStack.get().openOuter();

        assertTrue(outer::isOpen);
        assertEquals(0, TransactionStack.get().getDepth());
        
        Transaction nested = outer.openNested();
        assertTrue(nested::isOpen);
        assertEquals(1, TransactionStack.get().getDepth());
        
        assertThrowsExactly(IllegalStateException.class, outer::abort);
        
        assertDoesNotThrow(nested::abort);
        assertFalse(nested::isOpen);
        
        assertDoesNotThrow(outer::commit);
        assertFalse(outer::isOpen);

        assertEquals(-1, TransactionStack.get().getDepth());
    }
}
