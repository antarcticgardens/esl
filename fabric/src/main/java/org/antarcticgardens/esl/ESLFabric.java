package org.antarcticgardens.esl;

import net.fabricmc.api.ModInitializer;
import org.antarcticgardens.esl.energy.BlockEnergyStorageManager;
import org.antarcticgardens.esl.energy.ItemEnergyStorageManager;
import org.antarcticgardens.esl.fabric.energy.FabricBlockEnergyStorageManager;
import org.antarcticgardens.esl.fabric.energy.FabricItemEnergyStorageManager;
import org.antarcticgardens.esl.fabric.transaction.FabricTransactionStack;
import org.antarcticgardens.esl.transaction.TransactionStack;

import java.util.function.Supplier;

public class ESLFabric extends ESL implements ModInitializer {
    private final BlockEnergyStorageManager blockEnergyStorageManager = new FabricBlockEnergyStorageManager();
    private final ItemEnergyStorageManager itemEnergyStorageManager = new FabricItemEnergyStorageManager();
    
    @Override
    public void onInitialize() {
        
    }

    @Override
    public BlockEnergyStorageManager getBlockEnergyStorageManager() {
        return blockEnergyStorageManager;
    }

    @Override
    public ItemEnergyStorageManager getItemEnergyStorageManager() {
        return itemEnergyStorageManager;
    }

    @Override
    public Supplier<TransactionStack> getTransactionStackSupplier() {
        return FabricTransactionStack::new;
    }
}
