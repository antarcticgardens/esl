package org.antarcticgardens.esl.fabric.energy;

import org.antarcticgardens.esl.energy.EnergyStorage;
import org.antarcticgardens.esl.fabric.mixin.SnapshotParticipantInvoker;
import org.antarcticgardens.esl.fabric.transaction.FabricTransaction;
import org.antarcticgardens.esl.transaction.SnapshotParticipant;

import java.util.Map;
import java.util.WeakHashMap;

@SuppressWarnings({"unchecked", "UnstableApiUsage"})
public class F2EEnergyStorageAdapter extends SnapshotParticipant<Object> implements EnergyStorage {
    private static final Map<team.reborn.energy.api.EnergyStorage, F2EEnergyStorageAdapter> cache = new WeakHashMap<>();
    
    private final team.reborn.energy.api.EnergyStorage handle;
    private final SnapshotParticipantInvoker<Object> snapshotParticipantInvoker;
    
    F2EEnergyStorageAdapter(team.reborn.energy.api.EnergyStorage handle) {
        this.handle = handle;
        
        if (handle instanceof SnapshotParticipantInvoker<?> inv) {
            snapshotParticipantInvoker = (SnapshotParticipantInvoker<Object>) inv;
        } else {
            snapshotParticipantInvoker = null;
        }
    }
    
    public static F2EEnergyStorageAdapter getOrCreate(team.reborn.energy.api.EnergyStorage handle) {
        F2EEnergyStorageAdapter adapter = cache.get(handle);

        if (adapter != null) {
            return adapter;
        }

        adapter = new F2EEnergyStorageAdapter(handle);
        cache.put(handle, adapter);
        return adapter;
    }
    
    @Override
    public long insert(long amount, org.antarcticgardens.esl.transaction.TransactionContext transaction) {
        return handle.insert(amount, ((FabricTransaction) transaction).getHandle()); 
    }

    @Override
    public long extract(long amount, org.antarcticgardens.esl.transaction.TransactionContext transaction) {
        return handle.extract(amount, ((FabricTransaction) transaction).getHandle());
    }

    @Override
    public long getStoredEnergy() {
        return handle.getAmount();
    }

    @Override
    public long getCapacity() {
        return handle.getCapacity();
    }

    @Override
    public boolean supportsExtraction() {
        return handle.supportsExtraction();
    }

    @Override
    public boolean supportsInsertion() {
        return handle.supportsInsertion();
    }

    @Override
    public Object createSnapshot() {
        if (snapshotParticipantInvoker != null) {
            return snapshotParticipantInvoker.invokeCreateSnapshot();
        }
        
        return null;
    }

    @Override
    public void restoreSnapshot(Object snapshot) {
        if (snapshotParticipantInvoker != null) {
            snapshotParticipantInvoker.invokeReadSnapshot(snapshot);
        }
    }
}
