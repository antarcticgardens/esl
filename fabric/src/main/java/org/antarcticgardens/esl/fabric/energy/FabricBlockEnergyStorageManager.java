package org.antarcticgardens.esl.fabric.energy;

import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.antarcticgardens.esl.energy.EnergyStorage;
import org.antarcticgardens.esl.energy.BlockEnergyStorageManager;
import org.jetbrains.annotations.Nullable;

import java.util.function.BiFunction;

public class FabricBlockEnergyStorageManager extends BlockEnergyStorageManager {
    @Override
    public <T extends BlockEntity> void platformRegisterForBlockEntity(BiFunction<T, Direction, @Nullable EnergyStorage> provider, BlockEntityType<T> blockEntityType) {
        team.reborn.energy.api.EnergyStorage.SIDED.registerForBlockEntity(
                (blockEntity, direction) -> new E2FEnergyStorageAdapter(provider.apply(blockEntity, direction)), blockEntityType);
    }

    @Override
    public EnergyStorage platformFind(World world, BlockPos pos, @Nullable Direction direction) {
        team.reborn.energy.api.EnergyStorage storage = team.reborn.energy.api.EnergyStorage.SIDED.find(world, pos, direction);
        
        if (storage == null) {
            return null;
        }
        
        return F2EEnergyStorageAdapter.getOrCreate(storage);
    }
}
