package org.antarcticgardens.esl.fabric.energy;

import net.fabricmc.fabric.api.transfer.v1.context.ContainerItemContext;
import net.minecraft.item.ItemConvertible;
import org.antarcticgardens.esl.energy.ItemEnergyStorageManager;
import org.antarcticgardens.esl.fabric.util.ContextItemHolder;
import org.antarcticgardens.esl.fabric.util.ItemHolderStorage;
import org.antarcticgardens.esl.util.ItemHolder;
import team.reborn.energy.api.EnergyStorage;

import java.util.function.Function;

@SuppressWarnings("UnstableApiUsage")
public class FabricItemEnergyStorageManager extends ItemEnergyStorageManager {
    @Override
    protected void platformRegister(Function<ItemHolder, org.antarcticgardens.esl.energy.EnergyStorage> provider, ItemConvertible item) {
        EnergyStorage.ITEM.registerForItems((itemStack, context) -> 
                E2FEnergyStorageAdapter.getOrCreate(provider.apply(new ContextItemHolder(context))), item);
    }

    public org.antarcticgardens.esl.energy.EnergyStorage platformFind(ItemHolder holder) {
        EnergyStorage storage = EnergyStorage.ITEM.find(holder.toItemStack(), 
                ContainerItemContext.ofSingleSlot(new ItemHolderStorage(holder)));
        
        if (storage == null) {
            return null;
        }
        
        return F2EEnergyStorageAdapter.getOrCreate(storage);
    }
}
