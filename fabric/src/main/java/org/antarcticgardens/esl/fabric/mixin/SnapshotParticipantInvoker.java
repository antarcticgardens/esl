package org.antarcticgardens.esl.fabric.mixin;

import net.fabricmc.fabric.api.transfer.v1.transaction.base.SnapshotParticipant;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

@SuppressWarnings("UnstableApiUsage")
@Mixin(SnapshotParticipant.class)
public interface SnapshotParticipantInvoker<T> {
    @Invoker(value = "createSnapshot", remap = false)
    T invokeCreateSnapshot();
    
    @Invoker(value = "readSnapshot", remap = false)
    void invokeReadSnapshot(T snapshot);
}
