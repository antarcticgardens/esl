package org.antarcticgardens.esl.fabric.transaction;

import org.antarcticgardens.esl.transaction.Transaction;
import org.antarcticgardens.esl.transaction.TransactionResult;

@SuppressWarnings("UnstableApiUsage")
public class FabricTransaction implements Transaction {
    private final net.fabricmc.fabric.api.transfer.v1.transaction.Transaction handle;
    private boolean open = true;
    
    public FabricTransaction(net.fabricmc.fabric.api.transfer.v1.transaction.Transaction handle) {
        this.handle = handle;
    }

    @Override
    public void commit() {
        handle.commit();
        open = false;
    }

    @Override
    public void abort() {
        handle.abort();
        open = false;
    }

    @Override
    public void close() {
        handle.close();
    }

    @Override
    public boolean isOpen() {
        return open;
    }

    @Override
    public Transaction openNested() {
        return new FabricTransaction(handle.openNested());
    }

    @Override
    public int getNestingDepth() {
        return handle.nestingDepth();
    }

    @Override
    public void addCloseCallback(CloseCallback callback) {
        handle.addCloseCallback((transaction, result) -> callback.onClose(this, TransactionResult.valueOf(result.name())));
    }

    @Override
    public void addOuterCloseCallback(OuterCloseCallback callback) {
        handle.addOuterCloseCallback(result -> callback.onOuterClose(TransactionResult.valueOf(result.name())));
    }

    @Override
    public Transaction getOpenTransaction(int depth) {
        return new FabricTransaction(handle.getOpenTransaction(depth));
    }
    
    public net.fabricmc.fabric.api.transfer.v1.transaction.Transaction getHandle() {
        return handle;
    }
}
