package org.antarcticgardens.esl.fabric.transaction;

import net.fabricmc.fabric.api.transfer.v1.transaction.Transaction;
import net.fabricmc.fabric.api.transfer.v1.transaction.TransactionContext;
import org.antarcticgardens.esl.transaction.TransactionStack;

@SuppressWarnings({"UnstableApiUsage", "deprecation"})
public class FabricTransactionStack extends TransactionStack {
    @Override
    public org.antarcticgardens.esl.transaction.Transaction openOuter() {
        return new FabricTransaction(Transaction.openOuter());
    }

    @Override
    public org.antarcticgardens.esl.transaction.Transaction getCurrent() {
        TransactionContext current = Transaction.getCurrentUnsafe();
        
        if (!(current instanceof Transaction txn)) {
            return null;
        }
        
        return new FabricTransaction(txn);
    }

    @Override
    public int getDepth() {
        org.antarcticgardens.esl.transaction.Transaction current = getCurrent();
        
        if (current == null) {
            return -1;
        }
        
        return current.getNestingDepth();
    }
}
