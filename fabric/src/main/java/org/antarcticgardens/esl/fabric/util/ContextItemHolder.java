package org.antarcticgardens.esl.fabric.util;

import net.fabricmc.fabric.api.transfer.v1.context.ContainerItemContext;
import net.fabricmc.fabric.api.transfer.v1.item.ItemVariant;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import org.antarcticgardens.esl.fabric.transaction.FabricTransaction;
import org.antarcticgardens.esl.transaction.TransactionUtil;
import org.antarcticgardens.esl.util.ItemHolder;
import org.antarcticgardens.esl.util.Mth;

@SuppressWarnings("UnstableApiUsage")
public class ContextItemHolder implements ItemHolder {
    private final ContainerItemContext context;

    public ContextItemHolder(ContainerItemContext context) {
        this.context = context;
    }

    @Override
    public Item getItem() {
        return context.getItemVariant().getItem();
    }

    @Override
    public long getAmount() {
        return context.getAmount();
    }

    @Override
    public NbtCompound getNbt() {
        return context.getItemVariant().getNbt();
    }

    @Override
    public void setItem(Item item) {
        setVariant(ItemVariant.of(item, getNbt()));
    }

    @Override
    public void setAmount(long amount) {
        try (FabricTransaction t = (FabricTransaction) TransactionUtil.openOuterOrNested()) {
            if (context.getAmount() > amount) {
                context.extract(context.getItemVariant(), context.getAmount() - amount, t.getHandle());
            } else if (context.getAmount() < amount) {
                context.insert(context.getItemVariant(), amount - context.getAmount(), t.getHandle());
            }
        }
    }

    @Override
    public void setNbt(NbtCompound nbt) {
        setVariant(ItemVariant.of(getItem(), nbt));
    }
    
    public void setVariant(ItemVariant variant) {
        try (FabricTransaction t = (FabricTransaction) TransactionUtil.openOuterOrNested()) {
            context.exchange(variant, context.getAmount(), t.getHandle());
            t.commit();
        }
    }

    @Override
    public void fromItemStack(ItemStack stack) {
        setVariant(ItemVariant.of(stack));
        setAmount(stack.getCount());
    }

    @Override
    public ItemStack toItemStack() {
        return context.getItemVariant().toStack(Mth.longToIntSaturated(getAmount()));
    }
}
