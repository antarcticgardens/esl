package org.antarcticgardens.esl.fabric.util;

import net.fabricmc.fabric.api.transfer.v1.item.base.SingleStackStorage;
import net.minecraft.item.ItemStack;
import org.antarcticgardens.esl.util.ItemHolder;

@SuppressWarnings("UnstableApiUsage")
public class ItemHolderStorage extends SingleStackStorage {
    private final ItemHolder holder;
    
    public ItemHolderStorage(ItemHolder holder) {
        this.holder = holder;
    }
    
    @Override
    protected ItemStack getStack() {
        return holder.toItemStack();
    }

    @Override
    protected void setStack(ItemStack stack) {
        holder.fromItemStack(stack);
    }
}
