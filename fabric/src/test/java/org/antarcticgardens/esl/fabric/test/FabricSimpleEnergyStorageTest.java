package org.antarcticgardens.esl.fabric.test;

import org.antarcticgardens.esl.ESLFabric;
import org.antarcticgardens.esl.test.SimpleEnergyStorageTestBase;
import org.junit.jupiter.api.BeforeAll;

public class FabricSimpleEnergyStorageTest extends SimpleEnergyStorageTestBase {
    @BeforeAll
    static void setup() {
        new ESLFabric();
    }
}
