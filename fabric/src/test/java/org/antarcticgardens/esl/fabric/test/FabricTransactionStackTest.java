package org.antarcticgardens.esl.fabric.test;

import org.antarcticgardens.esl.ESLFabric;
import org.antarcticgardens.esl.test.TransactionStackTestBase;
import org.junit.jupiter.api.BeforeAll;

public class FabricTransactionStackTest extends TransactionStackTestBase {
    @BeforeAll
    static void setup() {
        new ESLFabric();
    }
}
