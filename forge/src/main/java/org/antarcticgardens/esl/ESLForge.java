package org.antarcticgardens.esl;

import net.minecraftforge.fml.common.Mod;
import org.antarcticgardens.esl.energy.BlockEnergyStorageManager;
import org.antarcticgardens.esl.energy.ItemEnergyStorageManager;
import org.antarcticgardens.esl.forge.energy.ForgeBlockEnergyStorageManager;
import org.antarcticgardens.esl.forge.energy.ForgeItemEnergyStorageManager;
import org.antarcticgardens.esl.forge.transaction.ForgeTransactionStack;
import org.antarcticgardens.esl.transaction.TransactionStack;

import java.util.function.Supplier;

@Mod("esl")
public class ESLForge extends ESL {
    private final BlockEnergyStorageManager blockEnergyStorageManager = new ForgeBlockEnergyStorageManager();
    private final ItemEnergyStorageManager itemEnergyStorageManager = new ForgeItemEnergyStorageManager();
    
    @Override
    public BlockEnergyStorageManager getBlockEnergyStorageManager() {
        return blockEnergyStorageManager;
    }

    @Override
    public ItemEnergyStorageManager getItemEnergyStorageManager() {
        return itemEnergyStorageManager;
    }

    @Override
    public Supplier<TransactionStack> getTransactionStackSupplier() {
        return ForgeTransactionStack::new;
    }
}
