package org.antarcticgardens.esl.forge.energy;

import net.minecraftforge.energy.IEnergyStorage;
import org.antarcticgardens.esl.energy.EnergyStorage;
import org.antarcticgardens.esl.util.Mth;
import org.antarcticgardens.esl.transaction.Transaction;
import org.antarcticgardens.esl.transaction.TransactionUtil;

import java.util.Map;
import java.util.WeakHashMap;

public class E2FEnergyStorageAdapter implements IEnergyStorage {
    private static final Map<EnergyStorage, E2FEnergyStorageAdapter> cache = new WeakHashMap<>();
    
    private final EnergyStorage handle;
    
    public E2FEnergyStorageAdapter(EnergyStorage handle) {
        this.handle = handle;
    }

    public static E2FEnergyStorageAdapter getOrCreate(EnergyStorage handle) {
        E2FEnergyStorageAdapter adapter = cache.get(handle);

        if (adapter != null) {
            return adapter;
        }

        adapter = new E2FEnergyStorageAdapter(handle);
        cache.put(handle, adapter);
        return adapter;
    }
    
    @Override
    public int receiveEnergy(int i, boolean bl) {
        try (Transaction t = TransactionUtil.openOuterOrNested()) {
            long inserted = handle.insert(i, t);
            
            if (!bl) {
                t.commit();
            }
            
            return Mth.longToIntSaturated(inserted);
        }
    }

    @Override
    public int extractEnergy(int i, boolean bl) {
        try (Transaction t = TransactionUtil.openOuterOrNested()) {
            long extracted = handle.extract(i, t);

            if (!bl) {
                t.commit();
            }

            return Mth.longToIntSaturated(extracted);
        }
    }

    @Override
    public int getEnergyStored() {
        return Mth.longToIntSaturated(handle.getStoredEnergy());
    }

    @Override
    public int getMaxEnergyStored() {
        return Mth.longToIntSaturated(handle.getCapacity());
    }

    @Override
    public boolean canExtract() {
        return handle.supportsExtraction();
    }

    @Override
    public boolean canReceive() {
        return handle.supportsInsertion();
    }
}
