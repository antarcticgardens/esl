package org.antarcticgardens.esl.forge.energy;

import net.minecraftforge.energy.IEnergyStorage;
import org.antarcticgardens.esl.energy.EnergyStorage;
import org.antarcticgardens.esl.util.Mth;
import org.antarcticgardens.esl.transaction.SnapshotParticipant;
import org.antarcticgardens.esl.transaction.TransactionContext;

import java.util.Map;
import java.util.WeakHashMap;

public class F2EEnergyStorageAdapter extends SnapshotParticipant<Long> implements EnergyStorage {
    private static final Map<IEnergyStorage, F2EEnergyStorageAdapter> cache = new WeakHashMap<>();
    
    private final IEnergyStorage handle;
    
    public F2EEnergyStorageAdapter(IEnergyStorage handle) {
        this.handle = handle;
    }
    
    public static F2EEnergyStorageAdapter getOrCreate(IEnergyStorage handle) {
        F2EEnergyStorageAdapter adapter = cache.get(handle);
        
        if (adapter != null) {
            return adapter;
        }
        
        adapter = new F2EEnergyStorageAdapter(handle);
        cache.put(handle, adapter);
        return adapter;
    }
    
    @Override
    public long insert(long amount, TransactionContext context) {
        updateSnapshots(context);
        return handle.receiveEnergy(Mth.longToIntSaturated(amount), false);
    }

    @Override
    public long extract(long amount, TransactionContext context) {
        updateSnapshots(context);
        return handle.extractEnergy(Mth.longToIntSaturated(amount), false);
    }

    @Override
    public long getStoredEnergy() {
        return handle.getEnergyStored();
    }

    @Override
    public long getCapacity() {
        return handle.getMaxEnergyStored();
    }

    @Override
    public boolean supportsExtraction() {
        return handle.canExtract();
    }

    @Override
    public boolean supportsInsertion() {
        return handle.canReceive();
    }

    @Override
    public Long createSnapshot() {
        return (long) handle.getEnergyStored();
    }

    @Override
    public void restoreSnapshot(Long snapshot) {
        // We can only HOPE it will have enough energy or capacity
        if (snapshot > getStoredEnergy()) {
            handle.receiveEnergy(Mth.longToIntSaturated(snapshot - getStoredEnergy()), false);
        } else if (snapshot < getStoredEnergy()) {
            handle.extractEnergy(Mth.longToIntSaturated(getStoredEnergy() - snapshot), false);
        }
    }
}
