package org.antarcticgardens.esl.forge.energy;

import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.energy.IEnergyStorage;
import org.antarcticgardens.esl.energy.BlockEnergyStorageManager;
import org.antarcticgardens.esl.energy.EnergyStorage;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;
import java.util.function.BiFunction;

public class ForgeBlockEnergyStorageManager extends BlockEnergyStorageManager {
    @Override
    protected <T extends BlockEntity> void platformRegisterForBlockEntity(BiFunction<T, Direction, @Nullable EnergyStorage> provider, BlockEntityType<T> blockEntityType) {
    }

    @Override
    public EnergyStorage platformFind(World world, BlockPos pos, @Nullable Direction direction) {
        BlockEntity be = world.getBlockEntity(pos);
        
        if (be == null) {
            return null;
        }
        
        Optional<IEnergyStorage> cap = be.getCapability(ForgeCapabilities.ENERGY, direction).resolve();
        return cap.map(F2EEnergyStorageAdapter::getOrCreate).orElse(null);
    }
}
