package org.antarcticgardens.esl.forge.energy;

import net.minecraft.item.ItemConvertible;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.energy.IEnergyStorage;
import org.antarcticgardens.esl.energy.EnergyStorage;
import org.antarcticgardens.esl.energy.ItemEnergyStorageManager;
import org.antarcticgardens.esl.util.ItemHolder;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;
import java.util.function.Function;

public class ForgeItemEnergyStorageManager extends ItemEnergyStorageManager {
    @Override
    protected void platformRegister(Function<ItemHolder, @Nullable EnergyStorage> provider, ItemConvertible item) {
    }

    @Override
    protected EnergyStorage platformFind(ItemHolder holder) {
        Optional<IEnergyStorage> cap = holder.toItemStack().getCapability(ForgeCapabilities.ENERGY).resolve();
        return cap.map(F2EEnergyStorageAdapter::getOrCreate).orElse(null);
    }
}
