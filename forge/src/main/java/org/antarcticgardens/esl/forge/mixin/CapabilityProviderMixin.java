package org.antarcticgardens.esl.forge.mixin;

import net.minecraft.block.entity.BlockEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityProvider;
import net.minecraftforge.common.util.LazyOptional;
import org.antarcticgardens.esl.ESL;
import org.antarcticgardens.esl.energy.EnergyStorage;
import org.antarcticgardens.esl.forge.energy.E2FEnergyStorageAdapter;
import org.antarcticgardens.esl.util.ItemStackHolder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@SuppressWarnings("UnstableApiUsage")
@Mixin(CapabilityProvider.class)
public class CapabilityProviderMixin {
    @Inject(method = "getCapability", at = @At("HEAD"), locals = LocalCapture.CAPTURE_FAILHARD, cancellable = true)
    public <T> void getCapability(@NotNull Capability<T> cap, @Nullable Direction side, CallbackInfoReturnable<LazyOptional<T>> cir) {
        if (((Object) this) instanceof BlockEntity be) {
            if (ESL.getInstance().getBlockEnergyStorageManager().isBlockEntityRegisteredInESL(be.getType())) {
                EnergyStorage storage = ESL.getInstance().getBlockEnergyStorageManager().find(be.getWorld(), be.getPos(), side);
                cir.setReturnValue(LazyOptional.of(() -> E2FEnergyStorageAdapter.getOrCreate(storage)).cast());
            }
        } else if (((Object) this) instanceof ItemStack stack) {
            if (ESL.getInstance().getItemEnergyStorageManager().isItemRegisteredInESL(stack.getItem())) {
                EnergyStorage storage = ESL.getInstance().getItemEnergyStorageManager().find(new ItemStackHolder(stack));
                cir.setReturnValue(LazyOptional.of(() -> E2FEnergyStorageAdapter.getOrCreate(storage)).cast());
            }
        }
    }
}
