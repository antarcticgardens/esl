package org.antarcticgardens.esl.forge.transaction;

import org.antarcticgardens.esl.ESL;
import org.antarcticgardens.esl.transaction.Transaction;
import org.antarcticgardens.esl.transaction.TransactionStack;
import org.antarcticgardens.esl.transaction.TransactionResult;

import java.util.ArrayList;
import java.util.List;

public class ForgeTransaction implements Transaction {
    private final List<CloseCallback> closeCallbacks = new ArrayList<>();
    private final List<OuterCloseCallback> outerCloseCallbacks = new ArrayList<>();
    private final Thread thread = Thread.currentThread();
    private final int depth = TransactionStack.get().getDepth() + 1;
    
    private boolean open = true;
    
    ForgeTransaction() {
        
    }
    
    @Override
    public void commit() {
        closeInternal(TransactionResult.COMMITTED);
    }

    @Override
    public void abort() {
        closeInternal(TransactionResult.ABORTED);
    }

    @Override
    public boolean isOpen() {
        return open;
    }

    @Override
    public void close() {
        if (isOpen()) {
            abort();
        }
    }
    
    private void closeInternal(TransactionResult result) {
        validateDepthAndThread();
        validateOpen();
        
        open = false;
        
        for (CloseCallback callback : closeCallbacks) {
            try {
                callback.onClose(this, result);
            } catch (Exception e) {
                ESL.LOGGER.error(e);
            }
        }

        if (depth == 0) {
            for (OuterCloseCallback callback : outerCloseCallbacks) {
                try {
                    callback.onOuterClose(result);
                } catch (Exception e) {
                    ESL.LOGGER.error(e);
                }
            }
        }

        ((ForgeTransactionStack) TransactionStack.get()).pop();
    }

    @Override
    public Transaction openNested() {
        validateDepthAndThread();
        return ((ForgeTransactionStack) TransactionStack.get()).open();
    }

    @Override
    public int getNestingDepth() {
        validateThread();
        return depth;
    }

    @Override
    public Transaction getOpenTransaction(int depth) {
        validateThread();
        return ((ForgeTransactionStack) TransactionStack.get()).getOpenTransaction(depth);
    }

    @Override
    public void addCloseCallback(CloseCallback callback) {
        validateThread();
        validateOpen();
        closeCallbacks.add(callback);
    }

    @Override
    public void addOuterCloseCallback(OuterCloseCallback callback) {
        validateThread();
        
        if (depth != 0) {
            getOpenTransaction(0).addOuterCloseCallback(callback);
        } else {
            outerCloseCallbacks.add(callback);
        }
    }
    
    private void validateDepthAndThread() {
        validateDepth();
        validateThread();
    }
    
    private void validateDepth() {
        if (depth != TransactionStack.get().getDepth()) {
            throw new IllegalStateException("You can only operate transaction on the top of transaction stack");
        }
    }
    
    private void validateThread() {
        if (thread != Thread.currentThread()) {
            throw new IllegalStateException("You can not operate transactions opened on other threads");
        }
    }
    
    private void validateOpen() {
        if (!isOpen()) {
            throw new IllegalStateException("You can not operate transactions that are already closed");
        }
    }
}
