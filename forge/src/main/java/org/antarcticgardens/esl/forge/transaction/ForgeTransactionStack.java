package org.antarcticgardens.esl.forge.transaction;

import org.antarcticgardens.esl.transaction.Transaction;
import org.antarcticgardens.esl.transaction.TransactionStack;

import java.util.Stack;

public class ForgeTransactionStack extends TransactionStack {
    private final Stack<ForgeTransaction> transactions = new Stack<>();
    
    @Override
    public Transaction openOuter() {
        if (!transactions.isEmpty()) {
            throw new IllegalStateException("There is already an outer transaction on this thread");
        }
        
        return open();
    }

    @Override
    public Transaction getCurrent() {
        return transactions.peek();
    }

    @Override
    public int getDepth() {
        return transactions.size() - 1;
    }
    
    Transaction open() {
        ForgeTransaction transaction = new ForgeTransaction();
        transactions.add(transaction);
        return transaction;
    }
    
    void pop() {
        transactions.pop();
    }
    
    Transaction getOpenTransaction(int depth) {
        return transactions.get(depth);
    }
}
