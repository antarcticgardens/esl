package org.antarcticgardens.esl.forge.test;

import org.antarcticgardens.esl.ESLForge;
import org.antarcticgardens.esl.test.SimpleEnergyStorageTestBase;
import org.junit.jupiter.api.BeforeAll;

public class ForgeSimpleEnergyStorageTest extends SimpleEnergyStorageTestBase {
    @BeforeAll
    static void setup() {
        new ESLForge();
    }
}
