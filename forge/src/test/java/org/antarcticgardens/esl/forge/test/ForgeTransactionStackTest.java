package org.antarcticgardens.esl.forge.test;

import org.antarcticgardens.esl.ESLForge;
import org.antarcticgardens.esl.test.TransactionStackTestBase;
import org.junit.jupiter.api.BeforeAll;

public class ForgeTransactionStackTest extends TransactionStackTestBase {
    @BeforeAll
    static void setup() {
        new ESLForge();
    }
}
